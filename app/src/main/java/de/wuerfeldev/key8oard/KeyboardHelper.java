package de.wuerfeldev.key8oard;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Point;
import android.util.Log;
import android.view.View;

public class KeyboardHelper {

    private Context ctx;

    private static Point center;
    private static float circlesize;

    KeyboardHelper(Context context){
        ctx = context;
    }

    public static Point measure(int x, int y, View caller){
        // Get size without mode
        Point p = new Point();
        p.x = View.MeasureSpec.getSize(x); //width
        p.y = View.MeasureSpec.getSize(y); //height

        // Get orientation
        if(caller.getResources().getConfiguration().orientation == android.content.res.Configuration.ORIENTATION_LANDSCAPE) {
            //landscape mode is bad -_-
            //TODO
        }else{  // Portrait mode
            // Adjust the height to match the aspect ratio 4:3
            //height = (int)(.75*width);
            //p.y = Math.round(0.75f * p.x);
            p.y = Math.round(0.75f * p.x);
        }

        calcCenter(p);
        return p;
    }

    private static void calcCenter(Point p){
        center = new Point(p.x/2,p.y/2);
        circlesize = Math.min(p.x,p.y)/6f;
    }

    public String[][][] getLetterArray(){
        Resources res = ctx.getResources();
        String[][][] letters = {
                { res.getStringArray(R.array.bar0a), res.getStringArray(R.array.bar0b) },
                { res.getStringArray(R.array.bar1a), res.getStringArray(R.array.bar1b) },
                { res.getStringArray(R.array.bar2a), res.getStringArray(R.array.bar2b) },
                { res.getStringArray(R.array.bar3a), res.getStringArray(R.array.bar3b) }};
        return letters;
    }


    public void setCenter(Point c,float size) {
        center = c;
        circlesize = size;
    }
    public Point getCenter(){
        return center;
    }
    public float getCirclesize(){
        return circlesize;
    }
}
