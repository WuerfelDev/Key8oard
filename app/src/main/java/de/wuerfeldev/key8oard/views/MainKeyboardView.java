package de.wuerfeldev.key8oard.views;

import android.content.Context;
import android.util.AttributeSet;
import android.view.ContextThemeWrapper;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.IdRes;

import de.wuerfeldev.key8oard.Key8oardIME;
import de.wuerfeldev.key8oard.R;

public class MainKeyboardView extends View {

    private Key8oardIME k8ime;
    private LinearLayout layout;
    private Button shiftbutton;


    public MainKeyboardView(Context context) {
        super(context);
        initialize(context);
    }
    public MainKeyboardView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initialize(context);
    }
    public MainKeyboardView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initialize(context);
    }

    private void initialize(Context context) {
        k8ime = (Key8oardIME) context;
        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        layout = (LinearLayout) inflater.inflate(R.layout.main_keyboard_view,null);

        createButton("←",new OnClickListener() {
            @Override
            public void onClick(View view) {
                k8ime.sendKey(KeyEvent.KEYCODE_DEL);
            }
        });
        createShiftButton();
        createButton("␣",new OnClickListener() {
            @Override
            public void onClick(View view) {
                k8ime.sendKey(KeyEvent.KEYCODE_SPACE);
            }
        });
        createButton("↵",new OnClickListener() {
            @Override
            public void onClick(View view) {
                k8ime.sendKey(KeyEvent.KEYCODE_ENTER);
            }
        });
    }

    public View getView() {
        return layout;
    }

    private void createButton(String label, OnClickListener ocl) {
        // Create Button and apply theme
        Button button = new Button(new ContextThemeWrapper(getContext(), R.style.ForButtons));
        button.setText(label);
        button.setOnClickListener(ocl);
        // Set weight not height, elements will stretch over parents height
        button.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,0,1));

        LinearLayout leftside = layout.findViewById(R.id.leftside);
        leftside.addView(button);
    }

    // Shift is special - it has different long click behaviour and changes icon when active ⬆ 🠭 ⇧
    private void createShiftButton(){ // Create Button and apply theme
        shiftbutton = new Button(new ContextThemeWrapper(getContext(), R.style.ForButtons));
        shiftbutton.setText("⇧");
        shiftbutton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                k8ime.setShift(Key8oardIME.Shift.ONCE);
            }
        });
        shiftbutton.setOnLongClickListener(new OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                k8ime.setShift(Key8oardIME.Shift.LOCK);
                return true;
            }
        });
        // Set weight not height, elements will stretch over parents height
        shiftbutton.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,0,1));

        LinearLayout leftside = layout.findViewById(R.id.leftside);
        leftside.addView(shiftbutton);
    }


    public void setBigLetter(String l){
        TextView tv = layout.findViewById(R.id.bigletter);
        tv.setText(l);
    }

    public void shiftChange(boolean onoff){
        if(onoff){
            shiftbutton.setText("⬆");
        }else{
            shiftbutton.setText("⇧");
        }
    }

}
