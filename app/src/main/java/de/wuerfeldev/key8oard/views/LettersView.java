package de.wuerfeldev.key8oard.views;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.view.View;

import de.wuerfeldev.key8oard.Key8oardIME;
import de.wuerfeldev.key8oard.KeyboardHelper;

public class LettersView extends View {

    Key8oardIME k8ime;


    public LettersView(Context context) {
        super(context);
        initialize(context);
    }
    public LettersView(Context context, AttributeSet attrs) {
        super(context,attrs);
        initialize(context);
    }
    public LettersView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initialize(context);
    }

    private void initialize(Context context) {
        k8ime = (Key8oardIME) context;
    }

    @Override
    public void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        Point p = KeyboardHelper.measure(widthMeasureSpec,heightMeasureSpec,this);
        setMeasuredDimension(p.x,p.y); //Protected function
    }


    @Override
    public void onDraw(Canvas c){
        //Log.v("LettersView",getWidth()+" w - h "+getHeight());

        /*Paint myPaint = new Paint();
        myPaint.setColor(Color.rgb(0, 0, 0));
        myPaint.setStrokeWidth(1);
        Rect r = new Rect(10, 10, c.getWidth() - 10, c.getHeight() - 10);
        c.drawRect(r, myPaint);*/
        String[][][] letters = k8ime.keyboardHelper.getLetterArray();
        // Switch "dir" of bar 1+3
        // Needed because positions are calculated and mirrored while drawing letters
        letters[1] = new String[][] {letters[1][1],letters[1][0]};
        letters[3] = new String[][] {letters[3][1],letters[3][0]};


        Point center = k8ime.keyboardHelper.getCenter();
        float circlesize = k8ime.keyboardHelper.getCirclesize();


        //int space = Math.min(getWidth(),getHeight())/20;

        Paint paint = new Paint();
        paint.setARGB(255, 100, 100, 100);

        // Font size
        float spscale = getContext().getResources().getDisplayMetrics().scaledDensity;
        paint.setTextSize(19*spscale);


        float circledist = circlesize * (float) Math.sin(Math.PI/4);

        int spacing = (int) (Math.min(getWidth(),getHeight())/2-circledist)/6;
        float odist = 1.5f*spacing; //orthogonal distance from bar


        for (int bar=0;bar<letters.length;bar++){ //every bar
            for (int dir=0;dir<letters[bar].length;dir++){ //every direction
                //int isodd = dir==0?-1:1;
                for (int dist=0;dist<letters[bar][dir].length;dist++){ //every char ->  distance from center
                    float rx = 0;// relative position
                    float ry = 0;

                    // Distance from center (size of circle + spacing for each letter)
                    rx = circledist + (dist) * spacing;
                    ry = circledist + (dist) * spacing;


                    // Position letters next to the bar not on it (up and right)
                    if(dir==0){
                        ry += odist;
                    }else{
                        rx += odist;
                    }

                    // Adjusting distance to match bars
                    if(bar==0||bar==3){ //upper bars
                        ry *= -1; //upwards
                    }
                    if(bar==2||bar==3){ //left bars
                        rx *= -1; //to the left
                    }


                    // debugging only
                    //c.drawCircle(center.x+rx,center.y+ry,10,paint);


                    // Center the letter
                    Rect bounds = new Rect();
                    paint.getTextBounds(letters[bar][dir][dist], 0, 1, bounds);
                    rx -= bounds.centerX();
                    ry -= bounds.centerY();


                    c.drawText(letters[bar][dir][dist],center.x+rx,center.y+ry,paint);

                }
            }
        }
    }
}