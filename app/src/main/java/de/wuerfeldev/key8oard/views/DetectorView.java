package de.wuerfeldev.key8oard.views;

import android.app.Activity;
import android.content.Context;
import android.graphics.Point;
import android.graphics.PointF;
import android.util.AttributeSet;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;

import java.util.LinkedList;

import de.wuerfeldev.key8oard.Key8oardIME;
import de.wuerfeldev.key8oard.KeyboardHelper;
import de.wuerfeldev.key8oard.R;

import static android.content.Context.INPUT_METHOD_SERVICE;

public class DetectorView extends View {

    private Key8oardIME k8ime;

    private LinkedList<Integer> history;
    private boolean typed;
    private int dirdist; //direction: +-, distance: int
    private int startfield;
    private String lastletter;



    public DetectorView(Context context) {
        super(context);
        initialize(context);
    }

    public DetectorView(Context context, AttributeSet attrs) {
        super(context,attrs);
        initialize(context);
    }
    public DetectorView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initialize(context);
    }

    private void initialize(Context context) {
        k8ime = (Key8oardIME) context;
        history = new LinkedList<>();
        typed = false;
        dirdist = 0;
        lastletter = "";
        startfield = 0;
    }

    @Override
    public void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        Point p = KeyboardHelper.measure(widthMeasureSpec,heightMeasureSpec,this);
        setMeasuredDimension(p.x,p.y); //Protected function
    }

    @Override
    public boolean onTouchEvent(MotionEvent e) {
        if(e.getActionIndex()!=0) return false;
        int field = wheresTheFinger(e.getX(0),e.getY(0)); //only first finger
        switch(e.getAction())
        {
            case MotionEvent.ACTION_DOWN:
                // initialise finger as first position
                cleanup();
                history.addFirst(field);
                break;
            case MotionEvent.ACTION_MOVE:
                // set finger movement history and type
                nextfield(field);
                break;
            case MotionEvent.ACTION_UP:
                //run logic (gestures)

                if(typed){ //Space after typing
                    if(field==0) k8ime.sendText(" ");
                }else{
                    gestures();
                }
                cleanup();
                break;
            case MotionEvent.ACTION_CANCEL:
                cleanup();
                break;
            default:
                return false;
        }
        return true;
    }


    /*

       2
       v
    3 >O< 1
       ^
       4

     */
    private int wheresTheFinger(float fingerx,float fingery){
        PointF relpos = new PointF();
        relpos.x = fingerx - k8ime.keyboardHelper.getCenter().x;
        relpos.y = k8ime.keyboardHelper.getCenter().y - fingery;

        // If in circle, return 0
        double d = Math.hypot(relpos.x,relpos.y);
        if(d<k8ime.keyboardHelper.getCirclesize()){
            return 0;
        }

        // Calculate angle, then return quarter
        double angle = Math.atan2(relpos.y, relpos.x);
        angle += Math.PI/4;
        if(angle < 0) {
            angle = Math.PI * 2 + angle;
        }
        return (int) (angle/(Math.PI/2))+1;
    }



    private void cleanup(){
        history.clear();
        dirdist = 0;
        typed = false;
        setLastLetter("");
    }

    private void setLastLetter(String l){
        lastletter = l;
        k8ime.mkv.setBigLetter(l);
    }

    public void nextfield(int field){
        if(!history.peekLast().equals(field)){ // moved to a new field
            if(history.peekFirst().equals(0)){ // first is 0, it could be a letter
                if(history.size()==1){ // currently first field after 0
                    startfield = field; // We calc the letters starting from here
                }else{
                    if (field == 0) {  // Moved 0-someting-0
                        if(lastletter.length()>0){ // dont send empty string
                            //vib(10);
                            k8ime.sendText(lastletter);
                        }
                        cleanup(); // reset for next character
                        typed = true; //prevents gesture detection after invalid typing (too many circles)
                    }else{
                        setLastLetter(calcLetter(field));
                    }
                }
            }
            history.addLast(field);
        }
    }


    private String calcLetter(int field){
        String[][][] letters = k8ime.keyboardHelper.getLetterArray();
        dirdist += direction(field);
        if(dirdist==0||Math.abs(dirdist)>8){ //out of bounds!
            return "";
        }

        int direction = dirdist<0?0:1;
        int bar = (6-(startfield+direction))%4; //its working at least
        int distance = (Math.abs(dirdist)-1)%4;
        String ret = letters[bar][direction][distance];
        if(Math.abs(dirdist)>4) ret=ret.toUpperCase();
        return ret;
    }


    private int direction(int field){
        switch (history.peekLast()){
            case 1:
                return field==2?1:-1;
            case 2:
                return field==3?1:-1;
            case 3:
                return field==4?1:-1;
            case 4:
                return field==1?1:-1;
        }
        return 0;
    }


    private void gestures(){
        switch (history.toString()){
            case "[0]":
                k8ime.sendText(" "); //insert space
                //after successful typing getHistory returns always [0] for the space
                break;
            case "[1]":
                k8ime.sendKey(KeyEvent.KEYCODE_DEL); //send backspace
                break;
            case "[2]":
                k8ime.setShift(Key8oardIME.Shift.ONCE);
                break;
            case "[3]":
                InputMethodManager imeManager = (InputMethodManager) getContext().getSystemService(INPUT_METHOD_SERVICE);
                imeManager.showInputMethodPicker(); //select input method
                break;
            case "[4]":
                k8ime.sendKey(KeyEvent.KEYCODE_ENTER); //send enter
                break;

            case "[2, 0, 4]":
                k8ime.requestHideSelf(0); // hide keyboard on swipe down
                break;
            case "[0, 1]":
                k8ime.sendKey(KeyEvent.KEYCODE_DPAD_RIGHT); // move cursor right
                break;
            case "[0, 2]":
                k8ime.sendKey(KeyEvent.KEYCODE_DPAD_UP); // move cursor up
                break;
            case "[0, 3]":
                k8ime.sendKey(KeyEvent.KEYCODE_DPAD_LEFT); // move cursor left
                break;
            case "[0, 4]":
                k8ime.sendKey(KeyEvent.KEYCODE_DPAD_DOWN); // move cursor down
                break;
        }
    }
}
