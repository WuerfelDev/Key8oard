package de.wuerfeldev.key8oard.views;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Point;
import android.util.AttributeSet;
import android.view.View;

import de.wuerfeldev.key8oard.Key8oardIME;
import de.wuerfeldev.key8oard.KeyboardHelper;

public class BackgroundView extends View {

    private Key8oardIME k8ime;

    private int outerxs,outerxe,outerys,outerye;

    public BackgroundView(Context context) {
        super(context);
        initialize(context);
    }
    public BackgroundView(Context context, AttributeSet attrs) {
        super(context,attrs);
        initialize(context);
    }
    public BackgroundView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initialize(context);
    }

    private void initialize(Context context) {
        k8ime = (Key8oardIME) context;
    }


    @Override
    public void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        Point p = KeyboardHelper.measure(widthMeasureSpec,heightMeasureSpec,this);
        calcBox(p.x,p.y);
        setMeasuredDimension(p.x,p.y); //Protected
    }


    private void calcBox(int x, int y) {
        outerxs = 0;
        outerxe = x;
        outerys = 0;
        outerye = y;
        if(x > y){ //wide keyboard (should be by default, line 61)
            int diff = (x-y)/2;
            outerxs = diff;
            outerxe -= diff;
        }else{
            int diff = (y-x)/2;
            outerys = diff;
            outerye -= diff;
        }
    }


    private void makeLines(Canvas canvas){
        int innerxs,innerxe,innerys,innerye;

        Paint paint = new Paint();
        paint.setARGB(255, 255, 255, 255);
        paint.setStyle(Paint.Style.FILL);
        paint.setStrokeWidth(10);
        int space = Math.min(canvas.getWidth(),canvas.getHeight())/20;
        // too short lines on espressowifi
        // https://github.com/flide/VI8/blob/master/app/src/main/java/inc/flide/vi8/views/mainKeyboard/XboardView.java


        innerxs=outerxs+space;
        innerxe=outerxe-space;
        innerys=outerys+space;
        innerye=outerye-space;

        //canvas.drawLine(hp+space,hp+space,lp-space,lp-space,paint);
        //canvas.drawLine(lp-space,hp+space,hp+space,lp-space,paint);
        canvas.drawLine(innerxs,innerys,innerxe,innerye,paint);
        canvas.drawLine(innerxs,innerye,innerxe,innerys,paint);


        //round line ending
        canvas.drawCircle(innerxs,innerys,5,paint);
        canvas.drawCircle(innerxs,innerye,5,paint);
        canvas.drawCircle(innerxe,innerys,5,paint);
        canvas.drawCircle(innerxe,innerye,5,paint);

    }

    @Override
    public void onDraw(Canvas c) {
        //Log.v("BackgroundView",getWidth()+" w - h "+getHeight());
        //super.onDraw(canvas);
        /*Paint myPaint = new Paint();
        myPaint.setColor(Color.rgb(0, 0, 0));
        myPaint.setStrokeWidth(1);
        Rect r = new Rect(10, 10, canvas.getWidth() - 10, canvas.getHeight() - 10);
        canvas.drawRect(r, myPaint);*/

        Point center = k8ime.keyboardHelper.getCenter();
        float circlesize = k8ime.keyboardHelper.getCirclesize();

        makeLines(c);
        Paint circlepaint = new Paint();
        circlepaint.setARGB(255, 255, 255, 255);
        circlepaint.setStyle(Paint.Style.FILL);
        c.drawCircle(center.x, center.y, circlesize, circlepaint);
    }
}
