package de.wuerfeldev.key8oard;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.inputmethodservice.InputMethodService;
import android.os.Bundle;
import android.provider.Settings;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void openSettings(View v){
        startActivity(new Intent(Settings.ACTION_INPUT_METHOD_SETTINGS));
    }

    public void selectKeyboard(View v) {
        InputMethodManager IMEManager = (InputMethodManager)getApplicationContext().getSystemService(Context.INPUT_METHOD_SERVICE);
        IMEManager.showInputMethodPicker();

        EditText textbar = (EditText) findViewById(R.id.textbar);
        textbar.requestFocus();
        textbar.setSelection(textbar.getText().length());
        IMEManager.showSoftInput(textbar, 0);
    }
}