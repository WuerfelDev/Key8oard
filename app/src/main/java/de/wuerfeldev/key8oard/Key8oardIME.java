package de.wuerfeldev.key8oard;

import android.content.Context;
import android.content.res.Configuration;
import android.inputmethodservice.InputMethodService;
import android.os.IBinder;
import android.os.SystemClock;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputConnection;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

import de.wuerfeldev.key8oard.views.MainKeyboardView;

public class Key8oardIME extends InputMethodService {

    public KeyboardHelper keyboardHelper;
    public MainKeyboardView mkv;
    private InputConnection inputConnection;


    public enum Shift {
        ONCE,
        LOCK,
        OFF
    }
    private Shift shift;




    @Override
    public void onInitializeInterface(){
        super.onInitializeInterface();
        inputConnection = getCurrentInputConnection();
    }

    @Override
    public void onBindInput() {
        inputConnection = getCurrentInputConnection();
    }

    @Override
    public void onStartInput (EditorInfo attribute, boolean restarting){
        super.onStartInput(attribute, restarting);
        inputConnection = getCurrentInputConnection();
    }

    @Override
    public View onCreateInputView() {
        keyboardHelper = new KeyboardHelper(getBaseContext());

        // Switch to some previous (some other) keyboard if landscape mode
        if(getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE){
            InputMethodManager inputMethodManager = (InputMethodManager) this.getSystemService(Context.INPUT_METHOD_SERVICE);
            IBinder iBinder = this.getWindow().getWindow().getAttributes().token;
            inputMethodManager.switchToLastInputMethod(iBinder);
            //inputMethodManager.toggleSoftInput(InputMethodManager.SHOW_FORCED,0);
            Toast.makeText(getBaseContext(),"Switching keyboard",Toast.LENGTH_SHORT).show();
        }
        mkv = (MainKeyboardView) getLayoutInflater().inflate(R.layout.main_keyboard_layout, null);
        return mkv.getView();
    }

    // Keyboard is shown
    @Override
    public void onStartInputView (EditorInfo info, boolean restarting){
        super.onStartInputView(info, restarting);
        // Reset to default start
        disableShift();
    }





    public void setShift(Shift s) { //EIther ONCE or LOCK but never called with OFF
        if(shift==Shift.LOCK || (shift==Shift.ONCE && s == Shift.ONCE)){
            disableShift();
        }else{
            shift = s;
            //TODO modify letterview + sidebar icon
            mkv.shiftChange(true);
        }
    }

    private void disableShift(){
        shift = Shift.OFF;
        //TODO modify letterview + sidebar icon
        mkv.shiftChange(false);
    }


    public void sendText(char c) {
        sendText(Character.toString(c));
    }
    public void sendText(String text) {
        if(shift!=Shift.OFF) text = text.toUpperCase();
        if(shift==Shift.ONCE) disableShift();

        inputConnection.commitText(text,text.length());// (text, newcursorpos -> length)
    }


    public void sendKey(int keyEventCode) {
        //XXX implement shift/flags?
        // Not useful atm (this is only called by sidebar and gestures)
        // Maybe relevant for future customizations
        sendKey(keyEventCode, 0);
    }
    public void sendKey(int keyEventCode , int flags) {
        sendDownAndUpKeyEvent(keyEventCode, flags);
    }


    public void sendDownAndUpKeyEvent(int keyEventCode, int flags){
        sendDownKeyEvent(keyEventCode, flags);
        sendUpKeyEvent(keyEventCode, flags);
    }

    public void sendDownKeyEvent(int keyEventCode, int flags) {
        inputConnection.sendKeyEvent(
                new KeyEvent(
                        SystemClock.uptimeMillis(),
                        SystemClock.uptimeMillis(),
                        KeyEvent.ACTION_DOWN,
                        keyEventCode,
                        0,
                        flags
                )
        );
    }

    public void sendUpKeyEvent(int keyEventCode, int flags) {
        inputConnection.sendKeyEvent(
                new KeyEvent(
                        SystemClock.uptimeMillis(),
                        SystemClock.uptimeMillis(),
                        KeyEvent.ACTION_UP,
                        keyEventCode,
                        0,
                        flags
                )
        );
    }

}
