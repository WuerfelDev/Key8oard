# Key8oard

Key8oard is an open-source remake of the long gone [8pen keyboard](http://www.8pen.com) for Android. The keyboard is very different from a usual QWERTY keyboard because it is build to support natural finger movement. Your regular keyboard was made to be used with 10 fingers - why do we still use those on smartphones too?
<!-- blank line -->
<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/99vsUF4NuLk" frameborder="0" allowfullscreen="true"> </iframe>
</figure>
<!-- blank line -->

## How to use it?
![Key8oard preview](images/key8oard.jpg)

### Typing
To type you always start in the center by holding your finger down and then move it into the field that contains the letter you want to write. Keep the finger down and move it from there around the circle in the direction of the letter. To select a letter that is displayed further away from the center, you need to move the finger over multiple fields. You can see a preview of the currently selected letter in the center. Finally to type that letter just move your finger back into the center.
To write multiple letters you can keep your finger down and just start with the next letter. Once you finish a word you can release the finger. If you release it in the center it will automatically add a space. If you don't want a space just move the finger out of the center and release it there.
![Typing r](images/typing_r.jpg)
![Typing hi](images/typing_hi.jpg)

To type letters in UPPERCASE you have two options: Do a full circle to select an uppercase letter or use the left-side button as a shift button.
![Typing M](images/typing_M.jpg)


### Gestures
You can move the cursor in a text field by swiping from the center to any direction.
![Cursor](images/cursor.jpg)

Another gesture is a swipe down to close the keyboard.

## Contribution

Your contribution of code, issues or feature requests is welcome :)

Special thanks to **flide** ([8VIM](https://github.com/flide/VI8/)) and CaptainBlagbird ([Xpen](https://github.com/CaptainBlagbird/Xpen)) who did prior work on an 8pen keyboard.
